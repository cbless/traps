# This file is part of trAPS.
# 
# trAPS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# trAPS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with trAPS.  If not, see <http://www.gnu.org/licenses/>.
'''
created on 26.09.2013

@author Christoph Bless
@license: GNU General Public License 3 or later

@see https://bitbucket.org/chbb/traps/wiki/Home

@summary: trAPS (APi Spy) is a Windows API Mointor. 
trAPS will wither loads an executable file or attachs to an existing process. 
In both cases trAPS installs hooks for some Windows API calls. 
For more details see the project wiki on https://bitbucket.org/chbb/traps/wiki/Home.

'''
import argparse
import sys 
import pkgutil
import logging 
import logging.config
from ConfigParser import SafeConfigParser

try:
    from pydbg import *
    from pydbg.defines import * 
    from utils import hooking
except: 
    print "[Error] You need pydbg and the utils package from paimei"
    print "[-] pydbg: https://github.com/OpenRCE/pydbg"
    print "[-] Paimei: https://github.com/OpenRCE/paimei"
    print "[-] see instructions on https://github.com/OpenRCE/sulley/wiki/Windows-Installation"
    sys.exit(-1)

import hooks
from hooks.defines import trAPSHook, toHex, trAPSException
        
class trAPS(object):
    """
    This is the main class of the API Spy.
    
    """
    
    
    
    def __init__(self, logging_config=None, config_file=None, verbose=False):
        self.dbg = pydbg()
        ## stores all loaded dlls
        # { 'name of the dll' : dll }
        self.loaded_dlls = {}
        ## stores the available hooks. 
        # { 'module': { 'func': trAPSHook } }
        self.available_hooks = {}
        ## stores all resolved functions
        # { return address : trAPSHook }
        self.symbols = {}
        self.hookcontainer = hooking.hook_container()
        # read logging configuration from a config file
        if logging_config != None:
            try:
                logging.config.fileConfig(logging_config)
            except Exception, e:
                err = "error on loading config file: {0} \n {1}"
                err = err.format(logging_config, e)
                raise trAPSException(err)
        self.logger = logging.getLogger()
        # load all subclasses from trAPSHook from the hooks package
        #self.loadHooks() 
        self.dbg.set_callback(LOAD_DLL_DEBUG_EVENT, self.handleLoadDLL)
        self.config_loaded = False
        ## stores all loaded config elements:
        # { name of the dll : { function1  : True, function2 : False } }
        self.config = {}
        if config_file != None:
            self.loadConfig(filename=config_file)
        self.verbose = verbose
    
    def getProcessListStr(self):
        """
        This function enumerates all active processes and produces 
        an output string like this one:
        
        ------------------------------------------------------------
        list of active processes
        ------------------------------------------------------------
          PID | Name of Process
        ------------------------------------------------------------
            0 | [System Process]
            4 | System
          368 | smss.exe
          584 | csrss.exe
          608 | winlogon.exe
          652 | services.exe
          664 | lsass.exe
        
        @type dbg: pydbg 
        @param dbg: instance of pydbg
        
        @rtype: string
        @return: a list of active processes 
        """
        processes = self.dbg.enumerate_processes()
        str_list = []
        str_list.append("-"*60)
        str_list.append("list of active processes")
        str_list.append("-"*60)
        str_list.append("%5s | %s" % ("PID","Name of Process"))
        str_list.append("-"*60)
        for pid, name in processes:
            str_list.append("%5d | %s" %(pid, name))
        str_list.append("-"*60)
        return "\n".join(str_list)
    
    
          
        
    
    def load(self, filename , args):
        """
        This function loads an executable file into the debugger. 
        
        @type filename: string
        @param filename: Full path to executable to load
        
        @type args: String
        @param args: Command line arguments to pass to debuggee  
        """
        self.dbg.load(filename, command_line = args)
        self.dbg.debug_set_process_kill_on_exit(True)
        msg = "{0} loaded into Debugger".format(filename)
        self.log(msg) 
        
                
    def attach(self, prog_name=None, pid=-1):
        """
        This function attachs the debugger to an existing process either by
        the name of the process or by his PID.
        - if prog_name is set, this function looks for an active process 
          with this name and attach the debugger to this process
        - if pid is not -1 this function attachs the debugger the process 
          with the specified PID
        
        @type prog_name: string
        @param prog_name: Name of the process to monitor
        
        @type pid: int
        @param pid: PID of the process to monitor    
        """
        if prog_name != None:
            # a prog name was specified. We try to find the PID of the process
            # and attach to this PID
            for p, name in self.dbg.enumerate_processes():
                if name == prog_name:
                    self.dbg.attach(p)
                    msg = "attached to {0} with pid: {1}".format(prog_name, p)
                    self.log(msg)
                    break
            else: 
                msg = "Process not Found: {0} ".format(prog_name)
                raise trAPSException(msg)
        else: 
            # no filename and no process name => we need a PID to attach to a process
            if pid < 0: # invalid pid
                msg = "invalid PID: {0}".format(pid)
                raise trAPSException(msg)
            else:
                self.dbg.attach(pid)
                msg = "attached to process with PID: {0}".format(pid)
                self.log(msg)
               
    def __getSubclasses(self, c):
        """
        Detect all subclasses of the given class. 
        
        @type c: class
        @param c: Superclass 
        """
        subclasses = c.__subclasses__()
        for d in list(subclasses):
            subclasses.extend(self.__getSubclasses(d))
        return subclasses
    
    def loadHooks(self):
        """
        This function detects all subclasses of trAPSHook in the hooks package 
        and initialize them.
        """
        for loader, mod_name , ispkg in pkgutil.iter_modules(hooks.__path__):
            # import all modules from the hooks package
            __import__("hooks."+ mod_name)
        # and then detect all subclasses of trAPSHook
        hook_classes = self.__getSubclasses(trAPSHook)
        for hc in hook_classes:
            try:
                obj = hc()
                obj.verbose = self.verbose
            except:
                pass
            else:
                if not obj.dll in self.available_hooks:
                    self.available_hooks[obj.dll] = {}
                self.available_hooks[obj.dll][obj.name] = obj
        
    def __registerHooks(self, module):
        """
        This function registers all available hooks for the given module.
        
        @type module: string
        @param module: Name of the module / dll 
        """
        if module in self.loaded_dlls and module in self.available_hooks:
            if self.config_loaded and not module in self.config:
                return
            for key, value in self.available_hooks[module].iteritems():
                if self.config_loaded and not key in self.config[module]:
                    continue
                self.__registerHook(value)
                            
    def __registerHook(self, hook):
        """
        This function registers a hook for the function specified by argument.
        
        @type name: trAPSHook
        @param name: Instance of a trAPSHook subclass  
        """
        if self.config_loaded: # check if a config exists
            if hook.dll in self.config and hook.name in self.config[hook.dll]:
                # the dll and the function exists in self.config
                # check if the hook is enabled
                if self.config[hook.dll][hook.name] == False:
                    return # the hook is disabled. Don' t register the hook
        addr = self.dbg.func_resolve(hook.dll, hook.name) 
        if addr != 0:
            self.hookcontainer.add(self.dbg, addr, hook.nargs, hook.enter_hook, hook.exit_hook)
            self.symbols[addr] = hook
            self.log("hook for {0} installed".format(hook.name))
    
    def debug_event_loop(self):
        try:
            self.dbg.debug_event_loop()
        except pdx:
            pass
        
    def loadConfig(self, filename="traps.conf"):
        """
        This function loads the specfied config file.
        
        Format of config file:
        <pre>
        [name of the dll]
        function1 = True
        function2 = False
        </pre>
        
        Example:
        <pre>
        [ws2_32.dll]
        send = True
        sendto = True
        
        [kernel32.dll]
        WriteFile = False
        </pre>
        
        
        @type filename: string
        @param filename: Name of the config file (incl. path) 
        """
        parser = SafeConfigParser()
        # override the function optionxform because we want the 
        # keys case sensitive
        parser.optionxform = lambda x: x
        parser.read(filename)
        
        for dll in parser.sections():
            for func in parser.options(dll):
                enabled = parser.getboolean(dll, func)
                if dll not in self.config:
                    self.config[dll] = {}
                self.config[dll][func] = enabled
        self.config_loaded = True
    
        
    def handleLoadDLL(self, dbg):
        """
        This function handles the LOAD_DLL_DEBUG_EVENT.
        
        @type dbg: pydbg
        @param dbg: instance of pydbg  
        """
        last_dll = self.dbg.system_dlls[-1]
        self.log( "DLL loaded: {0} {1} {2}".format(toHex(last_dll.base),last_dll.name, last_dll.path))
        self.loaded_dlls[last_dll.name] = last_dll
        self.__registerHooks(last_dll.name)
        return DBG_CONTINUE
    
    def log(self, msg, dbg=None): 
        if self.verbose:
            print "[+] "+ msg
        if dbg:
            ctx = dbg.dump_context()
            msg = msg + "\n{0}\n{1}\n".format(ctx, "-"*80)
        self.logger.info(msg)
        
def main(args):
    
    traps = trAPS(config_file=args.config,
                  logging_config=args.log_conf, 
                  verbose=args.verbose)
    if not args.file and not args.name and args.pid == -1:
        print "[-] you have to specify a filename, process or PID"
        print traps.getProcessListStr()
        sys.exit(-1)
    try:
        if args.file:
            traps.load(args.file, args.args)
        else:
            traps.attach(prog_name=args.name, pid=args.pid)
    except Exception , e:
        print "[-] {0}".format(e)
        sys.exit(-1)
    else:
        traps.loadHooks() 
        traps.debug_event_loop()
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="trAPS (APi Spy)")
    parser.add_argument("-f", "--file", required=False, default=None,
                        help="File to load into debugger")
    parser.add_argument("-n", "--name", required=False, default=None,
                    help="Name of the process to attach debugger")
    parser.add_argument("-p", "--pid", required=False, type=int, default=-1,
                        help="PID of the process to attach debugger")
    parser.add_argument("-v", "--verbose", required=False, action="store_true",
                        help="verbose output. Display function calls on stdout")
    parser.add_argument("-c", "--config", required=False, default="traps.conf",
                        help="Name of the config file")
    parser.add_argument("-l", "--log_conf", required=False, default="logging.conf",
                        help="Name of the logging config file")   
    parser.add_argument("-a", "--args", required=False, default=None,
                        help="Arguments passed to executeable on load")                     
    args = parser.parse_args()
    
    try:
        main(args)
    except KeyboardInterrupt:
        pass