# This file is part of trAPS.
# 
# trAPS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# trAPS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with trAPS.  If not, see <http://www.gnu.org/licenses/>.
'''
created on 28.09.2013

@author Christoph Bless
@license: GNU General Public License 3 or later

@see https://bitbucket.org/chbb/traps/wiki/Home

@summary: trAPS (APi Spy) is a Windows API Mointor. 
trAPS will wither loads an executable file or attachs to an existing process. 
In both cases trAPS installs hooks for some Windows API calls. 
For more details see the project wiki on https://bitbucket.org/chbb/traps/wiki/Home.

'''
from pydbg import *
from pydbg.defines import *
import struct 
import socket
from ctypes import *


from defines import trAPSHook, toHex     



def readPortAndIP(dbg, sockaddr):
    """
    This function reads the Port and IP from a struct sockaddr and 
    returns a Tuple with port , ipTuple and an IP as String
    
    @type dbg: pydbg
    @param dbg: instance of pydbg
    
    @type sockaddr: long
    @param sockaddr: address of the struct sockaddr
    
    @rtype: Tuple
    @return: a Tuple with port, ipTuple and an IP as String
      
    """
    sin_family = dbg.read(sockaddr, 2)
        
    hiByte = ord(dbg.read(sockaddr + 2 , 1))
    lowByte = ord(dbg.read(sockaddr + 3 , 1))
    port = hiByte * 256 + lowByte
        
    ipFirstByte = ord(dbg.read(sockaddr + 4 , 1))
    ipSecondByte = ord(dbg.read(sockaddr + 5 , 1))
    ipThirdByte = ord(dbg.read(sockaddr + 6 , 1))
    ipForthByte = ord(dbg.read(sockaddr + 7 , 1))
    
    ipTuple = (ipFirstByte, ipSecondByte, ipThirdByte, ipForthByte)
    ip = "%d.%d.%d.%d" % (ipTuple)
    return port , ipTuple, ip
      
class SendHook(trAPSHook):
    """
    This class contains the hook functions for the send function. 
    
    Syntax of send:
    int send(
      _In_  SOCKET s,
      _In_  const char *buf,
      _In_  int len,
      _In_  int flags
    );
    
    @see http://msdn.microsoft.com/en-us/library/windows/desktop/ms740149%28v=vs.85%29.aspx
    
    """
    def __init__(self, dll="ws2_32.dll", name="send", nargs=4):
        """
        initialize the class. 
        The function name is set to "send" and the number of arguments for 
        the send function is 4.
        """
        trAPSHook.__init__(self, dll, name=name, nargs=nargs)
        
    def func_enter(self, dbg, args):
        """
        This function is a callback for the send function.
        It reads the parameter directly from the stack and prints them
        to stdout.
        
        @type dbg: pydbg
        @param dbg: instance of pydbg
        """
        if dbg.dbg.u.Exception.dwFirstChance:
            return DBG_EXCEPTION_NOT_HANDLED
           
        msg = " args (len: {0} buffer: {1})"
        
        # .. we read the socket (1. arg), buffer address (2. arg) and length 
        # (3. arg) from the stack
        esp = dbg.context.Esp
        sock = struct.unpack("I", dbg.read_process_memory(esp + 0x04, 4))[0]        
        buffer_addr = struct.unpack("L", dbg.read_process_memory(esp + 0x08, 4))[0] 
        length = struct.unpack("L", dbg.read_process_memory(esp + 0x0C, 4)) [0]
    
        # read buffer from the buffer_addr
        buf = dbg.read_process_memory(buffer_addr, length) 
        msg = msg.format(length, buf, buf)
        self.log_func_enter(msg, dbg, args)  
        
        return DBG_CONTINUE




   
class SendtoHook(SendHook):
    """
    This class contains the hook functions for the sendto function. 
    
    Syntax of sendto:
    int sendto(
      _In_  SOCKET s,
      _In_  const char *buf,
      _In_  int len,
      _In_  int flags,
      _In_  const struct sockaddr *to,
      _In_  int tolen
    );
    
    @see http://msdn.microsoft.com/en-us/library/windows/desktop/ms740148%28v=vs.85%29.aspx
    
    """
    def __init__(self):
        SendHook.__init__(self, name="sendto", nargs=6)
   
   
   
        

class RecvHook(trAPSHook):
    """
    This class contains the hook functions for the recv function. 
    
    Syntax of recv:
    int recv(
      _In_   SOCKET s,
      _Out_  char *buf,
      _In_   int len,
      _In_   int flags
    );
    
    @see http://msdn.microsoft.com/en-us/library/windows/desktop/ms740121%28v=vs.85%29.aspx
            
    """
    def __init__(self, dll="ws2_32.dll", name="recv", nargs=4):
        trAPSHook.__init__(self, dll, name, nargs)
        self.buffer_addr = None
        
    def func_enter(self, dbg, args):
        """
        This function is a hook for the function recv.
        It reads the parameter passed to recv by reading them from stack.
        
        
        @type dbg: pydbg
        @param dbg: instance of pydbg
        """
        if dbg.dbg.u.Exception.dwFirstChance:
            return DBG_EXCEPTION_NOT_HANDLED
        
        # we read the return address, buffer address and length (number of bytes 
        # to receive) from the stack
        esp = dbg.context.Esp
        ret = struct.unpack("L", dbg.read_process_memory(esp,4))[0]                    # return address
        buffer_addr = struct.unpack("L", dbg.read_process_memory(esp + 0x08, 4))[0]    # second argument
        length = struct.unpack("<L", dbg.read_process_memory(esp + 0x0C, 4))[0]        # third argument
        # !!! it is possible to use args array to get the arguments. 
        # this is less voodoo. !!!
        # buffer_addr = args[2]
        msg = "args( buffer addr: {0}, length: {1})".format(toHex(buffer_addr), length)
        
        self.log_func_enter(msg, dbg, args) 
        # buf = dbg.smart_dereference(buffer_addr) # read received data from buffer_addr
        self.buffer_addr = buffer_addr
        
        
    def func_exit(self, dbg, args, ret):
        """
        this function is called after the recv() function to display the 
        received data. We need this exit hook because the buffer 
        will be filled during execution of the function recv. After recv()
        returns we are able to read the data from the buffer address (the 
        register EAX contains the number of received bytes). 
        
        @type dbg: pydbg
        @param dbg: instance of pydbg
        """
        if self.buffer_addr != None:
            # dbg.context.Eax contains the number of received bytes
            buf = dbg.read_process_memory(self.buffer_addr, dbg.context.Eax)
        msg = "received {0} bytes. buffer address: {1} data: {2}"
        msg = msg.format(dbg.context.Eax,toHex(self.buffer_addr), buf)
        self.buffer_addr = None
        self.log_func_exit(msg, dbg, args, ret) 
        return DBG_CONTINUE





class RecvfromHook(RecvHook):
    """
    This class contains the hook functions for the recvfrom function. 
    
    Syntax of recvfrom:
    int recvfrom(
      _In_         SOCKET s,
      _Out_        char *buf,
      _In_         int len,
      _In_         int flags,
      _Out_        struct sockaddr *from,
      _Inout_opt_  int *fromlen
    );
    
    @see http://msdn.microsoft.com/en-us/library/windows/desktop/ms740120%28v=vs.85%29.aspx
                   
    """
    def __init__(self, name="recvfrom", nargs=6):
        RecvHook.__init__(self, name, nargs)






class BindHook(trAPSHook):
    """
    This class contains the hook functions for the bind function. 
    
    Syntax of bind:
    int bind(
      _In_  SOCKET s,
      _In_  const struct sockaddr *name,
      _In_  int namelen
    );
    
    @see http://msdn.microsoft.com/en-us/library/windows/desktop/ms737550%28v=vs.85%29.aspx
    
    """
    def __init__(self, dll="ws2_32.dll", name="bind", nargs=3):
        """
        initialize the class. 
        The function name is set to "bind" and the number of arguments for 
        the send function is 3.
        """
        trAPSHook.__init__(self, dll, name=name, nargs=nargs)
        
    
    def func_enter(self, dbg, args):
        # arg 1 == socket, arg2 == address, arg3 == length of addr
        sockaddr = dbg.get_arg(2)
        length = dbg.get_arg(3)
        
        if length == 16:
            port, ipTuple, ip_str  = readPortAndIP(dbg, sockaddr)
            msg = "Port: {0}, IP: {1}, Length {2}"
            msg = msg.format(port, ip_str, length)
        else:
            msg = "args( addr: {0}, length: {1})".format(sockaddr, length)
        self.log_func_enter(msg, dbg, args)           
        return DBG_CONTINUE
        

class ConnectHook(trAPSHook):
    """
    This class contains the hook functions for the connect function. 
    
    Syntax of connect:
    int connect(
      _In_  SOCKET s,
      _In_  const struct sockaddr *name,
      _In_  int namelen
    );

    
    @see http://msdn.microsoft.com/en-us/library/windows/desktop/ms737625%28v=vs.85%29.aspx
    
    """
    def __init__(self, dll="ws2_32.dll", name="connect", nargs=4):
        """
        initialize the class. 
        The function name is set to "connect" and the number of arguments for 
        the send function is 3.
        """
        trAPSHook.__init__(self, dll, name=name, nargs=nargs)
        
    
    def func_enter(self, dbg, args):
        # arg 1 == socket, arg2 == address, arg3 == length of addr
        sockaddr = dbg.get_arg(2)
        length = dbg.get_arg(3)
        
        if length == 16:
            port, ipTuple, ip_str  = readPortAndIP(dbg, sockaddr)
            msg = "Port: {0}, IP: {1}, Length {2}"
            msg = msg.format(port, ip_str, length)
        else:
            msg = "args( addr: {0}, length: {1})".format(sockaddr, length)
        self.log_func_enter(msg, dbg, args)           
        return DBG_CONTINUE
    
