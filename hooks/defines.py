# This file is part of trAPS.
# 
# trAPS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# trAPS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with trAPS.  If not, see <http://www.gnu.org/licenses/>.
'''
created on 29.09.2013

@author Christoph Bless
@license: GNU General Public License 3 or later

@see https://bitbucket.org/chbb/traps/wiki/Home

@summary: trAPS (APi Spy) is a Windows API Mointor. 
trAPS will wither loads an executable file or attachs to an existing process. 
In both cases trAPS installs hooks for some Windows API calls. 
For more details see the project wiki on https://bitbucket.org/chbb/traps/wiki/Home.

'''
from pydbg import *
from pydbg.defines import *

import logging

class trAPSException(Exception):
    pass

class trAPSHook(object):
    """
    This is the base class for all hooks.
    """
    def __init__(self, dll, name, nargs, verbose=True):
        """
        initialize the trAPSHook
        
        @type dll: string
        @param dll: Name of the DLL
        
        @type name: string
        @param name: Name of the function
        
        @type nargs: int
        @param nargs: Number of arguments        
        """
        self.dll = dll
        self.name = name
        self.nargs = nargs
        self.enter_hook = self.func_enter
        self.exit_hook = self.func_exit
        self.logger = logging.getLogger("hooks")
        self.verbose = verbose
        msg_end = "{0}\n{1} End of Message {1}\n{0}\n"
        self.msg_end = msg_end.format("-"*95, "-"*40)

    def func_enter(self, dbg, args):
        """
        This function will be called when the hooked function is called.
        
        @type dbg: pydbg
        @param dbg: instance of pydbg
        
        @type args: list
        @param args: list of argsuments passed to the hooked routine
           
        """
        pass
    
    def func_exit(self, dbg, args, ret):
        """ 
        This function will be called when the hooked function returns.
        
        @type dbg: pydbg
        @param dbg: instance of pydbg
        
        @type args: list
        @param args: list of argsuments passed to the hooked routine
        
        @type ret:
        @param ret: return value of the hooked routine
        """
        pass

    
    def log_func_enter(self, msg, dbg=None, args=None):
        func = "{0}.{1} called!".format(self.dll, self.name)
        if self.verbose:
            print "[+] " + func
        
        if not args:
            arg_str = ""
        else:
            arg_str = "\nArguments: {0}".format(str(args))
            
        if not dbg:
            ctx =""
        else:
            ctx = dbg.dump_context()
        
        log_msg = "{0} (function enter hook){1}\nMessage: {2}\n{3}\n{4}"
        log_msg = log_msg.format(func, arg_str, msg, ctx, self.msg_end)
        self.logger.info(log_msg)
            
            
    def log_func_exit(self, msg, dbg=None, args=None, ret=None):
        func = "{0}.{1} called!".format(self.dll, self.name)
        
        if not args:
            arg_str = ""
        else:
            arg_str = "\nArguments: {0}".format(str(args))
        
        if not ret:
            ret_str = ""
        else:
            ret_str = "\nReturn value: {0}".format(ret)
        
        if not dbg:
            ctx =""
        else:
            ctx = "\n"+dbg.dump_context()
        
        log_msg = "{0} (function exit hook){1}{2}\nMessage: {3}{4}\n{5}"
        log_msg = log_msg.format(func, arg_str, ret_str, msg, ctx, self.msg_end)
        self.logger.info(log_msg)

def toHex(addr):
    """
    This function converts an address into a Hex string.
    
    @type addr: long 
    @param addr: memory address
    
    @rtype: string
    @return: Hex string
    """
    return "0x%08x" % addr 

        
def read_str(dbg, addr):
    """
    This function reads a String from the given address.
    
    @type dbg: pydbg
    @param dbg: instance of pydbg
    
    @type addr: long
    @param addr: start address  
     
    @rtype: string
    @return: the read string or string "N/A" on error
    
    @see pydbg.smart_dereference()
    """
    try:
        mbi = dbg.virtual_query(addr)
    except:
        return "N/A"
    
    if not mbi.Protect & PAGE_READWRITE:
        return "N/A"
            
    try:
        explored = dbg.read_process_memory(addr, 2024)
    except:
        return "N/A"
    
    explored_string = dbg.get_ascii_string(explored)
    
    if not explored_string:
        explored_string = dbg.get_unicode_string(explored)
    
    if not explored_string:
        explored_string = dbg.get_printable_string(explored)

    return explored_string       