# This file is part of trAPS.
# 
# trAPS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# trAPS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with trAPS.  If not, see <http://www.gnu.org/licenses/>.
'''
created on 30.09.2013

@author Christoph Bless
@license: GNU General Public License 3 or later

@see https://bitbucket.org/chbb/traps/wiki/Home

@summary: trAPS (APi Spy) is a Windows API Mointor. 
trAPS will wither loads an executable file or attachs to an existing process. 
In both cases trAPS installs hooks for some Windows API calls. 
For more details see the project wiki on https://bitbucket.org/chbb/traps/wiki/Home.

'''
from pydbg import *
from pydbg.defines import *
import struct 

from defines import trAPSHook, toHex



class WriteFileHook(trAPSHook):
    """
    This class contains the hook functions for the WriteFile function. 
    
    Syntax of send:
    
    BOOL WINAPI WriteFile(
      _In_         HANDLE hFile,
      _In_         LPCVOID lpBuffer,
      _In_         DWORD nNumberOfBytesToWrite,
      _Out_opt_    LPDWORD lpNumberOfBytesWritten,
      _Inout_opt_  LPOVERLAPPED lpOverlapped
    );
    
    @see http://msdn.microsoft.com/en-us/library/windows/desktop/aa365747%28v=vs.85%29.aspx
    
    """
    def __init__(self, dll="kernel32.dll", name="WriteFile", nargs=5):
        """
        initialize the class. 
        The function name is set to "WriteFile" and the number of arguments is 5.
        """
        trAPSHook.__init__(self, dll, name=name, nargs=nargs)
        
    def func_enter(self, dbg, args):
        """
        This function is a callback for the send function.
        It reads the parameter directly from the stack and prints them
        to stdout.
        
        @type dbg: pydbg
        @param dbg: instance of pydbg
        """
        if dbg.dbg.u.Exception.dwFirstChance:
            return DBG_EXCEPTION_NOT_HANDLED
        
        data = dbg.read_process_memory(dbg.get_arg(2), int(dbg.get_arg(3)))
        data = dbg.get_printable_string(data)
        msg = "HANDLE: {0}, lpBuffer: (1), nBytesToWrite: {2}, data: {3}"
        msg = msg.format(dbg.get_arg(0),dbg.get_arg(0),dbg.get_arg(0),data)
        
        self.log_func_enter(msg, dbg, args)
        
        return DBG_CONTINUE


class WriteFileExHook(WriteFileHook):
    """
    Syntax of WriteFileEx:
    
    BOOL WINAPI WriteFileEx(
      _In_      HANDLE hFile,
      _In_opt_  LPCVOID lpBuffer,
      _In_      DWORD nNumberOfBytesToWrite,
      _Inout_   LPOVERLAPPED lpOverlapped,
      _In_      LPOVERLAPPED_COMPLETION_ROUTINE lpCompletionRoutine
    );
    
    @see http://msdn.microsoft.com/en-us/library/windows/desktop/aa365748%28v=vs.85%29.aspx
    """
    def __init__(self, dll="kernel32.dll", name="WriteFileEx", nargs=5):
        """
        initialize the class. 
        The function name is set to "WriteFileEx" and the number of arguments is 5.
        """
        WriteFileHook.__init__(self, dll, name=name, nargs=nargs)
