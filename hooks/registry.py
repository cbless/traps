# This file is part of trAPS.
# 
# trAPS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# trAPS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with trAPS.  If not, see <http://www.gnu.org/licenses/>.
'''
created on 30.09.2013

@author Christoph Bless
@license: GNU General Public License 3 or later

@see https://bitbucket.org/chbb/traps/wiki/Home

@summary: trAPS (APi Spy) is a Windows API Mointor. 
trAPS will wither loads an executable file or attachs to an existing process. 
In both cases trAPS installs hooks for some Windows API calls. 
For more details see the project wiki on https://bitbucket.org/chbb/traps/wiki/Home.

'''
from pydbg import *
from pydbg.defines import *
import struct 

import _winreg

from defines import trAPSHook, toHex, read_str

"""

@see en.wikipedia.org/wiki/Windows_Registry

@see blogs.msdn.com/b/monad/archive/2006/03/08/546548.aspx

@see winreq.h
"""

# short form of the root keys
HKLM = _winreg.HKEY_LOCAL_MACHINE
HKCC = _winreg.HKEY_CURRENT_CONFIG
HKCR = _winreg.HKEY_CLASSES_ROOT
HKCU = _winreg.HKEY_CURRENT_USER

## dictonary to resolve values of the constants to readable names
HKEYS = {
         _winreg.HKEY_CLASSES_ROOT : "HKEY_CLASSES_ROOT",
         _winreg.HKEY_CURRENT_USER : "HKEY_CURRENT_USER",
         _winreg.HKEY_LOCAL_MACHINE : "HKEY_LOCAL_MACHINE",
         _winreg.HKEY_USERS : "HKEY_USERS",
         _winreg.HKEY_PERFORMANCE_DATA : "HKEY_PERFORMANCE_DATA",
         _winreg.HKEY_CURRENT_CONFIG : "HKEY_CURRENT_CONFIG",
         _winreg.HKEY_DYN_DATA : "HKEY_DYN_DATA"
         }

def hkey_to_str(hkey):
    """
    This function resolves the root HKEY in a human readable format.
    
    @type hkey: long
    @param hkey: value of the HKEY  
    """
    if hkey in HKEYS:
        return HKEYS[hkey]
    return toHex(hkey)




class RegCreateKeyHook(trAPSHook):
    """
    Syntax of RegCreateKey:
    
    LONG WINAPI RegCreateKey(
      _In_      HKEY hKey,
      _In_opt_  LPCTSTR lpSubKey,
      _Out_     PHKEY phkResult
    );
    
    @see http://msdn.microsoft.com/en-us/library/windows/desktop/ms724842%28v=vs.85%29.aspx
    """
    def __init__(self, dll="advapi32.dll", name="RegCreateKey", nargs=3):
        trAPSHook.__init__(self, dll, name, nargs)
        
    def func_enter(self, dbg, args):
        if dbg.dbg.u.Exception.dwFirstChance:
            return DBG_EXCEPTION_NOT_HANDLED
        hkey = args[0]
        lpSubKey = args[1]
        phkey = args[2]
        hkey_str = hkey_to_str(hkey)
        subkey = read_str(dbg, lpSubKey)
        msg = "args( HKEY: {0} ({1}), SubKey: {2} ({3}), phkResult: {4})"
        msg = msg.format(toHex(hkey), hkey_str, lpSubKey, subkey,  phkey)
        
        self.log_func_enter(msg, dbg, args)
        return DBG_CONTINUE
    
    
class RegCreateKeyWHook(RegCreateKeyHook):
    """
    This is a Hook for the RegCreateKey routine (Unicode version is called 
    RegCreateKeyW)
    
    @see RegCreateKeyHook. 
    """    
    def __init__(self, dll="advapi32.dll", name="RegCreateKeyW", nargs=3):
        RegCreateKeyHook.__init__(self, dll, name, nargs)
        
        
class RegCreateKeyAHook(RegCreateKeyHook):
    """
    This is a Hook for the RegCreateKey routine (ANSI version is called 
    RegCreateKeyA)
    
    @see RegCreateKeyHook. 
    """    
    def __init__(self, dll="advapi32.dll", name="RegCreateKeyA", nargs=3):
        RegCreateKeyHook.__init__(self, dll, name, nargs)        
        
        
class RegCreateKeyExHook(trAPSHook):
    """
    Syntax of RegCreateKeyEx:
    
    LONG WINAPI RegCreateKeyEx(
      _In_        HKEY hKey,
      _In_        LPCTSTR lpSubKey,
      _Reserved_  DWORD Reserved,
      _In_opt_    LPTSTR lpClass,
      _In_        DWORD dwOptions,
      _In_        REGSAM samDesired,
      _In_opt_    LPSECURITY_ATTRIBUTES lpSecurityAttributes,
      _Out_       PHKEY phkResult,
      _Out_opt_   LPDWORD lpdwDisposition
    );
    
    @see http://msdn.microsoft.com/en-us/library/windows/desktop/ms724844%28v=vs.85%29.aspx
    """
    def __init__(self, dll="advapi32.dll", name="RegCreateKeyEx", nargs=9):
        trAPSHook.__init__(self, dll, name, nargs)
        
    def func_enter(self, dbg, args):
        if dbg.dbg.u.Exception.dwFirstChance:
            return DBG_EXCEPTION_NOT_HANDLED
        hkey = args[0]
        lpSubKey = args[1]
        hkey_str = hkey_to_str(hkey)
        subkey = read_str(dbg, lpSubKey)
        msg = "args( HKEY: {0} ({1}), SubKey: {2} ({3}), ... )"
        msg = msg.format(toHex(hkey), hkey_str, lpSubKey, subkey)
        
        self.log_func_enter(msg, dbg, args)
        return DBG_CONTINUE
    
    
class RegOpenKeyHook(trAPSHook):     
    """
    Syntax of RegOpenKey:
    
    LONG WINAPI RegOpenKey(
      _In_      HKEY hKey,
      _In_opt_  LPCTSTR lpSubKey,
      _Out_     PHKEY phkResult
    );
    
    @see http://msdn.microsoft.com/en-us/library/windows/desktop/ms724895%28v=vs.85%29.aspx
    """
    def __init__(self, dll="advapi32.dll", name="RegOpenKey", nargs=3):
        trAPSHook.__init__(self, dll, name, nargs)
    
    def func_enter(self, dbg, args):
        if dbg.dbg.u.Exception.dwFirstChance:
            return DBG_EXCEPTION_NOT_HANDLED
        hkey = args[0]
        lpSubKey = args[1]
        hkey_str = hkey_to_str(hkey)
        subkey = read_str(dbg, lpSubKey)
        msg = "args( HKEY: {0} ({1}), SubKey: {2} ({3}), ... )"
        msg = msg.format(toHex(hkey), hkey_str, lpSubKey, subkey)
        
        self.log_func_enter(msg, dbg, args)
        return DBG_CONTINUE

class RegOpenKeyWHook(RegOpenKeyHook):     
    """
    Syntax of RegOpenKeyW (Unicode version of RegOpenKey):
    
    LONG WINAPI RegOpenKeyW(
      _In_      HKEY hKey,
      _In_opt_  LPCTSTR lpSubKey,
      _Out_     PHKEY phkResult
    );
    
    @see http://msdn.microsoft.com/en-us/library/windows/desktop/ms724895%28v=vs.85%29.aspx
    """
    def __init__(self, dll="advapi32.dll", name="RegOpenKeyW", nargs=3):
        RegOpenKeyHook.__init__(self, dll, name, nargs)
    

class RegOpenKeyAHook(RegOpenKeyHook):     
    """
    Syntax of RegOpenKeyA (ANSI version of RegOpenKey):
    
    LONG WINAPI RegOpenKeyA(
      _In_      HKEY hKey,
      _In_opt_  LPCTSTR lpSubKey,
      _Out_     PHKEY phkResult
    );
    
    @see http://msdn.microsoft.com/en-us/library/windows/desktop/ms724895%28v=vs.85%29.aspx
    """
    def __init__(self, dll="advapi32.dll", name="RegOpenKeyA", nargs=3):
        RegOpenKeyHook.__init__(self, dll, name, nargs)

    
class RegOpenKeyExHook(RegOpenKeyHook):     
    """
    Syntax of RegOpenKeyEx:
    
    LONG WINAPI RegOpenKeyEx(
      _In_        HKEY hKey,
      _In_opt_    LPCTSTR lpSubKey,
      _Reserved_  DWORD ulOptions,
      _In_        REGSAM samDesired,
      _Out_       PHKEY phkResult
    );

    @see http://msdn.microsoft.com/en-us/library/windows/desktop/ms724897%28v=vs.85%29.aspx
    """
    def __init__(self, dll="advapi32.dll", name="RegOpenKeyEx", nargs=5):
        RegOpenKeyHook.__init__(self, dll, name, nargs)
    
    

class RegGetValueHook(trAPSHook):     
    """
    Syntax of RegGetValue:
    
    LONG WINAPI RegGetValue(
      _In_         HKEY hkey,
      _In_opt_     LPCTSTR lpSubKey,
      _In_opt_     LPCTSTR lpValue,
      _In_opt_     DWORD dwFlags,
      _Out_opt_    LPDWORD pdwType,
      _Out_opt_    PVOID pvData,
      _Inout_opt_  LPDWORD pcbData
    );

    @see http://msdn.microsoft.com/en-us/library/windows/desktop/ms724868%28v=vs.85%29.aspx
    """
    def __init__(self, dll="advapi32.dll", name="RegGetValue", nargs=7):
        trAPSHook.__init__(self, dll, name, nargs)
    
    def func_enter(self, dbg, args):
        if dbg.dbg.u.Exception.dwFirstChance:
            return DBG_EXCEPTION_NOT_HANDLED
        hkey = args[0]
        lpSubKey = args[1]
        lpValue = args[2]
        hkey_str = hkey_to_str(hkey)
        subkey = read_str(dbg, lpSubKey)
        value = read_str(dbg, lpValue)
        msg = "args( HKEY: {0} ({1}), lpSubKey: {2} ({3}),  lpValue {4}... )"
        msg = msg.format(toHex(hkey), hkey_str, lpSubKey, subkey, value)
        
        self.log_func_enter(msg, dbg, args)
        return DBG_CONTINUE
    

class RegGetValueWHook(RegGetValueHook):     
    """
    Syntax of RegGetValueW (Unicode version of RegGetValue):
    
    LONG WINAPI RegGetValueW(
      _In_         HKEY hkey,
      _In_opt_     LPCTSTR lpSubKey,
      _In_opt_     LPCTSTR lpValue,
      _In_opt_     DWORD dwFlags,
      _Out_opt_    LPDWORD pdwType,
      _Out_opt_    PVOID pvData,
      _Inout_opt_  LPDWORD pcbData
    );

    @see http://msdn.microsoft.com/en-us/library/windows/desktop/ms724868%28v=vs.85%29.aspx
    """
    def __init__(self, dll="advapi32.dll", name="RegGetValueW", nargs=7):
        trAPSHook.__init__(self, dll, name, nargs)

class RegGetValueAHook(RegGetValueHook):     
    """
    Syntax of RegGetValueA (ANSI version of RegGetValue):
    
    LONG WINAPI RegGetValueA(
      _In_         HKEY hkey,
      _In_opt_     LPCTSTR lpSubKey,
      _In_opt_     LPCTSTR lpValue,
      _In_opt_     DWORD dwFlags,
      _Out_opt_    LPDWORD pdwType,
      _Out_opt_    PVOID pvData,
      _Inout_opt_  LPDWORD pcbData
    );

    @see http://msdn.microsoft.com/en-us/library/windows/desktop/ms724868%28v=vs.85%29.aspx
    """
    def __init__(self, dll="advapi32.dll", name="RegGetValueA", nargs=7):
        trAPSHook.__init__(self, dll, name, nargs)
        

class RegDeleteKeyHook(trAPSHook):     
    """
    Syntax of RegDeleteKey:
    
    LONG WINAPI RegDeleteKey(
      _In_  HKEY hKey,
      _In_  LPCTSTR lpSubKey
    );

    @see http://msdn.microsoft.com/en-us/library/windows/desktop/ms724845%28v=vs.85%29.aspx
    """
    def __init__(self, dll="advapi32.dll", name="RegDeleteKey", nargs=2):
        trAPSHook.__init__(self, dll, name, nargs)
        
    def func_enter(self, dbg, args):
        if dbg.dbg.u.Exception.dwFirstChance:
            return DBG_EXCEPTION_NOT_HANDLED
        hkey = args[0]
        lpSubKey = args[1]
        hkey_str = hkey_to_str(hkey)
        subkey = read_str(dbg, lpSubKey)
        msg = "args( HKEY: {0} ({1}), LPCTSTR: {2} ({3})"
        msg = msg.format(toHex(hkey), hkey_str, lpSubKey, subkey)
        
        self.log_func_enter(msg, dbg, args)
        return DBG_CONTINUE


class RegDeleteKeyWHook(RegDeleteKeyHook):     
    """
    Syntax of RegDeleteKeyW (Unicode version of RegDeleteKey):
    
    LONG WINAPI RegDeleteKeyW(
      _In_  HKEY hKey,
      _In_  LPCTSTR lpSubKey
    );

    @see http://msdn.microsoft.com/en-us/library/windows/desktop/ms724845%28v=vs.85%29.aspx
    """
    def __init__(self, dll="advapi32.dll", name="RegDeleteKeyW", nargs=2):
        trAPSHook.__init__(self, dll, name, nargs)


class RegDeleteKeyAHook(RegDeleteKeyHook):     
    """
    Syntax of RegDeleteKeyA (ANSI version of RegDeleteKey):
    
    LONG WINAPI RegDeleteKeyA(
      _In_  HKEY hKey,
      _In_  LPCTSTR lpSubKey
    );

    @see http://msdn.microsoft.com/en-us/library/windows/desktop/ms724845%28v=vs.85%29.aspx
    """
    def __init__(self, dll="advapi32.dll", name="RegDeleteKeyA", nargs=2):
        trAPSHook.__init__(self, dll, name, nargs)        
        
        
        
class RegDeleteKeyExHook(RegDeleteKeyHook):     
    """
    Syntax of RegDeleteKeyEx:
    
    LONG WINAPI RegDeleteKeyEx(
      _In_        HKEY hKey,
      _In_        LPCTSTR lpSubKey,
      _In_        REGSAM samDesired,
      _Reserved_  DWORD Reserved
    );

    @see http://msdn.microsoft.com/en-us/library/windows/desktop/ms724847%28v=vs.85%29.aspx
    """
    def __init__(self, dll="advapi32.dll", name="RegDeleteKeyEx", nargs=4):
        trAPSHook.__init__(self, dll, name, nargs)      